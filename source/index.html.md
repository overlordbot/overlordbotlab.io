---
title: OverlordBot
---

<!-- home section -->
<div id="home">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-3">
				<img src="images/OverlordBot-Full-512.png" class="img-responsive" alt="OverlordBot Logo">
			</div>
			<div class="col-md-7 col-sm-9">
			    <br/><br/><br/><br/><br/><br/>
				<h1>Voice Controlled Bot</h1>
				<h1>For DCS World</h1>
			</div>
		</div>
	</div>
</div>

<!-- divider section -->
<div class="divider">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6">
		    	<a href="awacs.html">
					<div class="divider-wrapper divider-one">
						<i class="fa fa-fighter-jet"></i>
						<h2>AWACS</h2>
						<p>Bogey dopes, warning announcements and other navigational assistance</p>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-6">
				<a href="atc.html">
					<div class="divider-wrapper divider-two">
						<i class="fa fa-building"></i>
						<h2>ATC</h2>
						<p>Inbound and Outbound ATC with de-confliction of taxiing and landing traffic</p>
					</div>
				</a>
			</div>
			<div class="col-md-4 col-sm-12">
		    	<a href="developer.html">
					<div class="divider-wrapper divider-three">
						<i class="fa fa-user"></i>
						<h2>Custom</h2>
						<p>Take advantage of Open Source and add your own custom commands to the bot</p>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>

<!-- Youtube Video
<div id="trailer">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ELmi_VitwQg" title="YouTube video player" frameborder="0"
				allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>
-->
